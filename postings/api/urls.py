from django.conf.urls import url

from .views import BlogPostRudView, BlogPostAPIView

app_name="postings"
urlpatterns = [
    url(r'^$', BlogPostAPIView.as_view(), name='post-listcreate'), # get and create view mixins
    url(r'^(?P<pk>\d+)/$', BlogPostRudView.as_view(), name='post-rud')  
]  